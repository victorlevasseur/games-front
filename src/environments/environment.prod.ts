export const environment = {
  production: true,
  wsUrl: 'ws://silk-bahamut.ddns.net:8080/games-backend/ws',
  apiUrl: 'http://silk-bahamut.ddns.net:8080/games-backend',
};
