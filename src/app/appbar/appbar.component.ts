import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserInfoService } from '../services/user-info.service';
import { User } from '../model/user';
import { Subscription } from 'rxjs';
import { ResolveEnd, Router } from '@angular/router';
import { RouterTools } from '../tools/router-tools';

@Component({
  selector: 'app-appbar',
  templateUrl: './appbar.component.html',
  styleUrls: ['./appbar.component.sass'],
})
export class AppbarComponent implements OnInit, OnDestroy {
  me: User | undefined;

  backToGamesListButtonDisplayed = false;

  private subscriptions: Subscription[] = [];

  constructor(
    private userInfoService: UserInfoService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.userInfoService.me$.subscribe((user) => (this.me = user))
    );
    this.subscriptions.push(
      this.router.events.subscribe((event) => {
        if (event instanceof ResolveEnd) {
          this.backToGamesListButtonDisplayed =
            RouterTools.getRecursiveData(event.state.root).backToGamesList ===
            true;
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
