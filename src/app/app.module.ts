import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterComponent } from './register/register.component';
import { GameFilterPipe } from './game-filter.pipe';
import { GamesComponent } from './games/games.component';
import { GameHeaderComponent } from './game-header/game-header.component';

import { TeamComponent } from './code-names/game/teams/team/team.component';
import { BoardComponent } from './code-names/game/board/board.component';
import { TeamsComponent } from './code-names/game/teams/teams.component';
import { SpyComponent } from './code-names/game/teams/team/spy/spy.component';
import { CodeNamesGameDetailComponent } from './code-names/game/game-detail/game-detail.component';
import { ValidationModalComponent } from './code-names/game/board/validation-modal/validation-modal.component';

import { UnoCardComponent } from './uno/game/board/card/uno-card.component';
import { UnoGameDetailComponent } from './uno/game/game-detail/game-detail.component';
import { UnoPlayersComponent } from './uno/game/players/uno-players.component';
import { UnoBoardComponent } from './uno/game/board/uno-board.component';
import { ColorSelectionModalComponent } from './uno/game/board/card/color-selection-modal/color-selection-modal.component';
import { UnoColorButtonComponent } from './uno/game/board/color-button/color-button.component';

import { ExplodingGameDetailComponent } from './exploding/game/game-detail/game-detail.component';
import { ExplodingPlayersComponent } from './exploding/game/players/exploding-players.component';
import { ExplodingBoardComponent } from './exploding/game/board/exploding-board.component';
import { UserInfoService } from './services/user-info.service';
import { WebsocketProviderService } from './services/websocket-provider.service';
import { ActionsService } from './code-names/services/actions.service';
import { GameGuard } from './code-names/game/game.guard';
import { UserGuard } from './user.guard';
import { GameService } from './code-names/services/game.service';
import { UnoActionsService } from './uno/services/uno-actions.service';
import { UnoGameService } from './uno/services/uno-game.service';
import { UnoGameGuard } from './uno/game/uno-game.guard';
import { ExplodingActionsService } from './exploding/service/exploding-actions.service';
import { ExplodingGameService } from './exploding/service/exploding-game.service';
import { ExplodingGameGuard } from './exploding/exploding-game.guard';
import { ExplodingCardComponent } from './exploding/game/board/card/exploding-card.component';
import { PlayerSelectionModalComponent } from './exploding/game/board/player-selection-modal/player-selection-modal.component';
import { BombReplaceModalComponent } from './exploding/game/board/bomb-replace-modal/bomb-replace-modal.component';
import { CardChooseModalComponent } from './exploding/game/board/card-choose-modal/card-choose-modal.component';
import { TypeSelectionModalComponent } from './exploding/game/board/type-selection-modal/type-selection-modal.component';
import { CDK_DRAG_CONFIG, DragDropModule } from '@angular/cdk/drag-drop';
import { HelpPageComponent } from './exploding/game/help-page/help-page.component';
import { HelpCardComponent } from './exploding/game/help-page/help-card/help-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialImportModule } from './material-import/material-import.module';
import { UiModule } from './ui/ui.module';
import { AppbarComponent } from './appbar/appbar.component';
import { GameItemComponent } from './games/game-item/game-item.component';
import { UnoPlayersStatsComponent } from './uno/game/players/uno-players-stats/uno-players-stats.component';
import { DecimalPipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    GameFilterPipe,
    GamesComponent,
    GameHeaderComponent,

    CodeNamesGameDetailComponent,
    TeamsComponent,
    TeamComponent,
    BoardComponent,
    SpyComponent,
    ValidationModalComponent,

    UnoCardComponent,
    UnoGameDetailComponent,
    UnoPlayersComponent,
    UnoBoardComponent,
    ColorSelectionModalComponent,
    UnoColorButtonComponent,

    ExplodingGameDetailComponent,
    ExplodingPlayersComponent,
    ExplodingBoardComponent,
    ExplodingCardComponent,
    PlayerSelectionModalComponent,
    BombReplaceModalComponent,
    CardChooseModalComponent,
    TypeSelectionModalComponent,
    HelpPageComponent,
    HelpCardComponent,
    AppbarComponent,
    GameItemComponent,
    UnoPlayersStatsComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    NgbModalModule,
    NgbDropdownModule,
    DragDropModule,
    BrowserAnimationsModule,
    MaterialImportModule,
    UiModule,
    FormsModule,
  ],
  providers: [
    WebsocketProviderService,
    CookieService,
    UserInfoService,
    UserGuard,

    ActionsService,
    GameService,
    GameGuard,

    UnoActionsService,
    UnoGameService,
    UnoGameGuard,

    ExplodingActionsService,
    ExplodingGameService,
    ExplodingGameGuard,

    {
      provide: CDK_DRAG_CONFIG,
      useValue: {
        dragStartThreshold: 0,
        pointerDirectionChangeThreshold: 5,
        zIndex: 10000,
      },
    },

    DecimalPipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
