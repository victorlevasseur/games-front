export interface User {
  name?: string;
  icon?: string;
  uuid: string;
  colorSetting?: string;
}
