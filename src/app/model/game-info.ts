import { GameType } from './game-type';

export interface GameInfo {
  type: GameType;
  guuid: string;
  name: string;
  joinable: boolean;
  playerNames: string[];
}
