import { User } from '../../model/user';
import { ExplodingNotifiedCard } from './exploding-notified-card';

export interface ExplodingNotifiedPlayer extends User {
  self: boolean;
  lastWinner: boolean;
  cursed: boolean;
  cards: ExplodingNotifiedCard[];
}
