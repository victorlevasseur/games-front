import { ExplodingNotifiedPlayer } from './exploding-notified-player';
import { ExplodingNotifiedCard } from './exploding-notified-card';
import { ExplodingCardType } from './exploding-card-type';

export interface ExplodingGame {
  uuid: string;
  name: string;
  players: ExplodingNotifiedPlayer[];
  cards: ExplodingNotifiedCard[];
  otherCards?: ExplodingNotifiedCard[];
  cardsPlayed?: ExplodingNotifiedCard[];
  previousTurn?: ExplodingNotifiedCard[];

  targetedPlayer?: string;
  cardDrawn?: ExplodingNotifiedCard;
  lastPlayed?: ExplodingNotifiedCard;
  nextDrawCard?: ExplodingNotifiedCard;
  nopeCount: number;
  currentCard?: ExplodingCardType;
  wantedCard?: ExplodingCardType;

  lastNope?: string;
  gameStarted: boolean;

  drawPileSize: number;
  drawResolver?: string;

  canDraw: boolean;
  canNope: boolean;
  shouldGiveCard: boolean;
  shouldChooseTarget: boolean;
}
