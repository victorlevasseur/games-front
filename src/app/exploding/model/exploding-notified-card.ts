import { ExplodingCardType } from './exploding-card-type';

export interface ExplodingNotifiedCard {
  uuid: string;
  type: ExplodingCardType;
  marked?: boolean;
}
