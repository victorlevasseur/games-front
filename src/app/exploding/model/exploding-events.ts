export enum ExplodingEvents {
  new_game = 'new_game',
  add_player = 'add_player',
  shuffle_players = 'shuffle_players',
  launch_game = 'launch_game',

  play_card = 'play_card',
  nope_played = 'nope_played',
  resolve_card = 'resolve_card',
  post_card_action = 'post_card_action',

  offer_target = 'offer_target',
  noped = 'noped',
  validated = 'validated',

  draw_card = 'draw_card',
  defuse = 'defuse',
  keep_card = 'keep_card',
  die = 'die',
  cursed_defuse = 'cursed_defuse',
}
