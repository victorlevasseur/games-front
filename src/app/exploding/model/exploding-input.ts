import { ExplodingCardType } from './exploding-card-type';

export interface ExplodingInput {
  cardUuid?: string;
  cardType?: ExplodingCardType;
  targetPlayerUuid?: string;
  targetPosition?: number;
  nopeCount?: number;
}
