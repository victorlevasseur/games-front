import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { User } from '../../model/user';
import { DEFAULT_HEADER } from '../../model/http-header';
import { GameType, GameTypePath } from '../../model/game-type';
import { ExplodingEvents } from '../model/exploding-events';
import { ExplodingGame } from '../model/exploding-game';
import { ExplodingInput } from '../model/exploding-input';

@Injectable({ providedIn: 'root' })
export class ExplodingActionsService {
  private readonly baseUrl: string;

  constructor(private httpClient: HttpClient) {
    this.baseUrl =
      environment.apiUrl + '/' + GameTypePath.get(GameType.EXPLODING);
  }

  updateUser(guuid: string, user: User): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${user.uuid}/${ExplodingEvents.add_player}`,
      user,
      DEFAULT_HEADER
    );
  }

  replayUser(guuid: string, uuid: string): Observable<ExplodingGame> {
    return this.httpClient.get<ExplodingGame>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/replay`,
      DEFAULT_HEADER
    );
  }

  launchGame(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.launch_game}`,
      null,
      DEFAULT_HEADER
    );
  }

  shufflePlayers(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.shuffle_players}`,
      null,
      DEFAULT_HEADER
    );
  }

  playCard(
    guuid: string,
    uuid: string,
    body?: ExplodingInput
  ): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.play_card}`,
      body,
      DEFAULT_HEADER
    );
  }

  nope(guuid: string, uuid: string, body: ExplodingInput): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.nope_played}`,
      body,
      DEFAULT_HEADER
    );
  }

  resolve(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.validated}`,
      null,
      DEFAULT_HEADER
    );
  }

  offerTarget(
    guuid: string,
    uuid: string,
    targetUuid: string
  ): Observable<void> {
    const body: ExplodingInput = {
      targetPlayerUuid: targetUuid,
    };
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.offer_target}`,
      body,
      DEFAULT_HEADER
    );
  }

  drawCard(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.draw_card}`,
      null,
      DEFAULT_HEADER
    );
  }

  postCardAction(
    guuid: string,
    uuid: string,
    body?: ExplodingInput
  ): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.post_card_action}`,
      body,
      DEFAULT_HEADER
    );
  }

  keepCard(guuid: string, uuid: string) {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.keep_card}`,
      null,
      DEFAULT_HEADER
    );
  }

  defuse(guuid: string, uuid: string, body?: ExplodingInput) {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.defuse}`,
      body,
      DEFAULT_HEADER
    );
  }

  die(guuid: string, uuid: string) {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.die}`,
      null,
      DEFAULT_HEADER
    );
  }

  cursedDefuse(
    guuid: string,
    uuid: string,
    body?: ExplodingInput
  ): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.cursed_defuse}`,
      body,
      DEFAULT_HEADER
    );
  }

  replaceCursedBomb(
    guuid: string,
    uuid: string,
    body?: ExplodingInput
  ): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${ExplodingEvents.resolve_card}`,
      body,
      DEFAULT_HEADER
    );
  }
}
