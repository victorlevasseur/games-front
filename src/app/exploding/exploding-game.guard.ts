import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserInfoService } from '../services/user-info.service';
import { ExplodingActionsService } from './service/exploding-actions.service';
import { ExplodingGameService } from './service/exploding-game.service';
import { ExplodingGame } from './model/exploding-game';

@Injectable({ providedIn: 'root' })
export class ExplodingGameGuard implements CanActivate {
  constructor(
    private gameService: ExplodingGameService,
    private actionsService: ExplodingActionsService,
    private userInfoService: UserInfoService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> {
    if (this.gameService.gameId != null) {
      return of(true);
    } else {
      const guuid = route.params.guuid;
      return this.actionsService
        .replayUser(guuid, this.userInfoService.uuid)
        .pipe(
          map((notifiedGame) => {
            if (notifiedGame != null) {
              this.gameService.gameId = guuid;
              this.gameService.publishGame(notifiedGame);
            }
            return this.router.createUrlTree(
              this.buildRedirect(notifiedGame, guuid)
            );
          })
        );
    }
  }

  redirect(unoGame: ExplodingGame, guuid): void {
    const route = this.buildRedirect(unoGame, guuid);
    const tree = this.router.createUrlTree(route);
    if (!this.router.isActive(tree, true)) {
      this.router.navigate(route);
    }
  }

  private buildRedirect(explodingGame: ExplodingGame, guuid: string): any[] {
    if (explodingGame != null) {
      if (!explodingGame.gameStarted) {
        return ['/exploding', 'game', guuid, 'players'];
      } else {
        return ['/exploding', 'game', guuid, 'board'];
      }
    } else {
      return ['/exploding', 'games'];
    }
  }
}
