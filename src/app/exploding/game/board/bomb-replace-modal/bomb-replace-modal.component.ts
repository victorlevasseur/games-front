import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bomb-replace-modal',
  templateUrl: './bomb-replace-modal.component.html',
  styleUrls: ['./bomb-replace-modal.component.sass'],
})
export class BombReplaceModalComponent {
  private _drawPileSize: number;
  formGroup: FormGroup;

  constructor(public modal: NgbActiveModal, private fb: FormBuilder) {}

  @Input()
  set drawPileSize(value: number) {
    this._drawPileSize = value;
    this.formGroup = this.fb.group({
      selectedPosition: [
        null,
        [Validators.required, Validators.min(0), Validators.max(value)],
      ],
    });
  }

  get drawPileSize(): number {
    return this._drawPileSize;
  }

  onSave() {
    if (this.formGroup.valid) {
      this.modal.close(this.formGroup.value.selectedPosition);
    }
  }
}
