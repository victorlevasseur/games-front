import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component } from '@angular/core';
import { ExplodingCardType } from '../../../model/exploding-card-type';

@Component({
  selector: 'app-type-selection-modal',
  templateUrl: './type-selection-modal.component.html',
  styleUrls: ['./type-selection-modal.component.sass'],
})
export class TypeSelectionModalComponent {
  types: ExplodingCardType[];

  constructor(public modal: NgbActiveModal) {
    this.types = Object.keys(ExplodingCardType)
      .filter((value) => {
        return (
          value !== ExplodingCardType.DOUBLE_CARD &&
          value !== ExplodingCardType.TRIPLE_CARD &&
          value !== ExplodingCardType.QUINTUPLE_CARD &&
          value !== ExplodingCardType.EXPLODING &&
          value !== ExplodingCardType.IMPLODING
        );
      })
      .map((value) => ExplodingCardType[value]);
  }
}
