import { Component, OnDestroy, OnInit } from '@angular/core';
import { ExplodingActionsService } from '../../service/exploding-actions.service';
import { ExplodingGameService } from '../../service/exploding-game.service';
import { Subscription } from 'rxjs';
import { ExplodingGame } from '../../model/exploding-game';
import { UserInfoService } from '../../../services/user-info.service';
import { ExplodingNotifiedPlayer } from '../../model/exploding-notified-player';
import { ExplodingCardType } from '../../model/exploding-card-type';
import { ExplodingNotifiedCard } from '../../model/exploding-notified-card';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlayerSelectionModalComponent } from './player-selection-modal/player-selection-modal.component';
import { BombReplaceModalComponent } from './bomb-replace-modal/bomb-replace-modal.component';
import { CardChooseModalComponent } from './card-choose-modal/card-choose-modal.component';
import { TypeSelectionModalComponent } from './type-selection-modal/type-selection-modal.component';
import { ExplodingGameGuard } from '../../exploding-game.guard';

@Component({
  selector: 'app-exploding-board',
  templateUrl: './exploding-board.component.html',
  styleUrls: ['./exploding-board.component.sass'],
})
export class ExplodingBoardComponent implements OnInit, OnDestroy {
  game: ExplodingGame;
  me: ExplodingNotifiedPlayer;
  selfTurn: boolean;
  waitingCards: string[];
  param: ExplodingCardType;
  target: ExplodingNotifiedPlayer;
  ExplodingCardType = ExplodingCardType;
  Array = Array;
  private gameSub: Subscription;
  private previousChooseCard = false;

  constructor(
    private actionsService: ExplodingActionsService,
    private gameService: ExplodingGameService,
    private userInfoService: UserInfoService,
    private gameGuard: ExplodingGameGuard,
    private _modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.gameSub = this.gameService.game$.subscribe((game) => {
      this.game = game;
      this.me = game.players.find((p) => p.uuid === this.userInfoService.uuid);
      this.selfTurn = game.players[0].self;
      if (!this.me) {
        this.me = <ExplodingNotifiedPlayer>this.userInfoService.me;
      } else {
        this.param = null;
        if (game.targetedPlayer != null) {
          this.target = game.players.find(
            (p) => p.uuid === game.targetedPlayer
          );
        } else {
          this.target = null;
        }
        if (game.shouldChooseTarget) {
          this.playerModal().then((value: string) => {
            this.actionsService
              .offerTarget(this.gameService.gameId, this.me.uuid, value)
              .subscribe();
          });
        }
        if (
          ((game.shouldGiveCard && game.cards.length > 0) || game.otherCards) &&
          !this.previousChooseCard
        ) {
          this.previousChooseCard = true;
          this.cardModal(
            game.shouldGiveCard ? game.cards : game.otherCards,
            game.currentCard
          ).then((value: string) => {
            this.actionsService
              .postCardAction(this.gameService.gameId, this.me.uuid, {
                cardUuid: value,
              })
              .subscribe();
            this.previousChooseCard = false;
          });
        }
        if (
          this.game.drawResolver == this.me.uuid &&
          this.game.cardDrawn &&
          this.game.cardDrawn.type == ExplodingCardType.EXPLODING &&
          this.game.lastPlayed &&
          this.game.lastPlayed.type == ExplodingCardType.DEFUSE
        ) {
          this.positionModal().then((value) => {
            this.actionsService
              .replaceCursedBomb(this.gameService.gameId, this.me.uuid, {
                targetPosition: value,
              })
              .subscribe();
          });
        }
      }
      this.gameGuard.redirect(game, this.gameService.gameId);
    });
  }

  ngOnDestroy(): void {
    this.gameSub.unsubscribe();
  }

  setParam(param: ExplodingCardType) {
    if (param !== this.param) {
      this.param = param;
      this.waitingCards = [];
    }
  }

  playCard(event: { added: boolean; card: ExplodingNotifiedCard }) {
    if (
      this.me.cursed &&
      this.game.cardDrawn &&
      this.game.cardDrawn.type == ExplodingCardType.EXPLODING &&
      this.canDie()
    ) {
      if (
        !this.game.lastPlayed ||
        this.game.lastPlayed.type != ExplodingCardType.DEFUSE
      ) {
        this.actionsService
          .cursedDefuse(this.gameService.gameId, this.me.uuid, {
            cardUuid: event.card.uuid,
          })
          .subscribe();
      }
    } else if (this.param == null) {
      switch (event.card.type) {
        case ExplodingCardType.TARGETED_ATTACK:
        case ExplodingCardType.CURSE_OF_THE_CAT_BUTT:
        case ExplodingCardType.FAVOR:
        case ExplodingCardType.MARK:
          this.playerModal().then((value: string) => {
            this.actionsService
              .playCard(this.gameService.gameId, this.me.uuid, {
                cardUuid: event.card.uuid,
                targetPlayerUuid: value,
              })
              .subscribe();
          });
          break;
        case ExplodingCardType.NOPE:
          if (this.game.canNope) {
            this.actionsService
              .nope(this.gameService.gameId, this.me.uuid, {
                cardUuid: event.card.uuid,
                nopeCount: this.game.nopeCount,
              })
              .subscribe();
          }
          break;
        default:
          this.actionsService
            .playCard(this.gameService.gameId, this.me.uuid, {
              cardUuid: event.card.uuid,
            })
            .subscribe();
          break;
      }
    } else {
      if (event.added) {
        this.waitingCards.push(event.card.uuid);
      } else {
        this.waitingCards = this.waitingCards.filter(
          (value) => value != event.card.uuid
        );
      }
      switch (this.param) {
        case ExplodingCardType.DOUBLE_CARD:
          if (this.waitingCards.length == 2) {
            this.playerModal().then((value) => {
              this.actionsService
                .playCard(this.gameService.gameId, this.me.uuid, {
                  cardUuid: this.waitingCards.join(','),
                  targetPlayerUuid: value,
                })
                .subscribe();
            });
          }
          break;
        case ExplodingCardType.TRIPLE_CARD:
          if (this.waitingCards.length == 3) {
            this.playerModal().then((value) => {
              this.typeModal().then((type) => {
                this.actionsService
                  .playCard(this.gameService.gameId, this.me.uuid, {
                    cardUuid: this.waitingCards.join(','),
                    targetPlayerUuid: value,
                    cardType: type,
                  })
                  .subscribe();
              });
            });
          }
          break;
        case ExplodingCardType.QUINTUPLE_CARD:
          if (this.waitingCards.length == 5) {
            this.typeModal().then((value) => {
              this.actionsService
                .playCard(this.gameService.gameId, this.me.uuid, {
                  cardUuid: this.waitingCards.join(','),
                  cardType: value,
                })
                .subscribe();
            });
          }
          break;
      }
    }
  }

  canDrawCard() {
    return this.selfTurn && this.game.cardDrawn == null;
  }

  drawCard() {
    this.actionsService
      .drawCard(this.gameService.gameId, this.me.uuid)
      .subscribe();
  }

  canKeepCard() {
    return (
      this.game.drawResolver == this.me.uuid &&
      this.game.cardDrawn != null &&
      this.game.cardDrawn.type != ExplodingCardType.IMPLODING
    );
  }

  keepCard() {
    this.actionsService
      .keepCard(this.gameService.gameId, this.me.uuid)
      .subscribe();
  }

  canDefuse() {
    const card = this.game.cardDrawn;
    return (
      this.game.drawResolver == this.me.uuid &&
      !this.me.cursed &&
      this.game.cardDrawn != null &&
      (card.type == ExplodingCardType.EXPLODING ||
        (card.type == ExplodingCardType.IMPLODING && !card.marked))
    );
  }

  defuse() {
    if (this.canDefuse()) {
      this.positionModal().then((value: number) => {
        this.actionsService
          .defuse(this.gameService.gameId, this.me.uuid, {
            targetPosition: value,
          })
          .subscribe();
      });
    }
  }

  canDie() {
    const card = this.game.cardDrawn;
    return (
      this.game.drawResolver == this.me.uuid &&
      card != null &&
      (card.type == ExplodingCardType.EXPLODING ||
        (card.type == ExplodingCardType.IMPLODING && card.marked))
    );
  }

  die() {
    if (this.canDie()) {
      this.actionsService
        .die(this.gameService.gameId, this.me.uuid)
        .subscribe();
    }
  }

  displayCards(player: ExplodingNotifiedPlayer) {
    this.cardModal(player.cards, null);
  }

  private playerModal(): Promise<string> {
    const modalRef = this._modalService.open(PlayerSelectionModalComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.players = this.game.players;
    return modalRef.result;
  }

  private cardModal(
    cards: ExplodingNotifiedCard[],
    currentCard: ExplodingCardType
  ): Promise<string> {
    const modalRef = this._modalService.open(CardChooseModalComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.cards = cards;
    modalRef.componentInstance.currentCard = currentCard;
    return modalRef.result;
  }

  private typeModal(): Promise<ExplodingCardType> {
    return this._modalService.open(TypeSelectionModalComponent, {
      backdrop: 'static',
    }).result;
  }

  private positionModal(): Promise<number> {
    const modalRef = this._modalService.open(BombReplaceModalComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.drawPileSize = this.game.drawPileSize;
    return modalRef.result;
  }
}
