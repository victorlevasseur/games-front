import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input } from '@angular/core';
import { ExplodingNotifiedCard } from '../../../model/exploding-notified-card';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ExplodingCardType } from '../../../model/exploding-card-type';

@Component({
  selector: 'app-card-choose-modal',
  templateUrl: './card-choose-modal.component.html',
  styleUrls: ['./card-choose-modal.component.sass'],
})
export class CardChooseModalComponent {
  @Input() public cards: ExplodingNotifiedCard[];
  @Input() public currentCard: ExplodingCardType;

  constructor(public modal: NgbActiveModal) {}

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.cards, event.previousIndex, event.currentIndex);
  }

  onClick(card: ExplodingNotifiedCard) {
    if (this.canPlay(card)) {
      this.modal.close(card.uuid);
    }
  }

  onClose() {
    this.modal.close(this.cards.map((c) => c.uuid).join(','));
  }

  canPlay(card: ExplodingNotifiedCard) {
    return (
      !this.canValidate() &&
      (this.currentCard != ExplodingCardType.MARK || !card.marked)
    );
  }

  canDrag() {
    return (
      this.currentCard == ExplodingCardType.ALTER_THE_FUTURE_3 ||
      this.currentCard == ExplodingCardType.ALTER_THE_FUTURE_5
    );
  }

  canValidate() {
    return (
      this.canDrag() ||
      this.currentCard == ExplodingCardType.SEE_THE_FUTURE_3 ||
      this.currentCard == ExplodingCardType.SEE_THE_FUTURE_5
    );
  }
}
