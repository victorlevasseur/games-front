import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input } from '@angular/core';
import { ExplodingNotifiedPlayer } from '../../../model/exploding-notified-player';

@Component({
  selector: 'app-player-selection-modal',
  templateUrl: './player-selection-modal.component.html',
  styleUrls: ['./player-selection-modal.component.sass'],
})
export class PlayerSelectionModalComponent {
  @Input() public players: ExplodingNotifiedPlayer[];

  constructor(public modal: NgbActiveModal) {}
}
