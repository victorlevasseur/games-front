import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ExplodingNotifiedCard } from '../../../model/exploding-notified-card';

@Component({
  selector: 'app-exploding-card',
  templateUrl: './exploding-card.component.html',
  styleUrls: ['./exploding-card.component.sass'],
})
export class ExplodingCardComponent {
  private _card: ExplodingNotifiedCard;
  played = false;
  type: string;
  @Input() playable = false;

  @Output() clicked = new EventEmitter<{
    added: boolean;
    card: ExplodingNotifiedCard;
  }>();

  get card(): ExplodingNotifiedCard {
    return this._card;
  }

  @Input()
  set card(value: ExplodingNotifiedCard) {
    this._card = value;
    this.type = value.type ? value.type : 'BACK';
  }

  onCardClicked() {
    if (this.playable) {
      this.played = !this.played;
      this.clicked.emit({ added: this.played, card: this._card });
    }
  }
}
