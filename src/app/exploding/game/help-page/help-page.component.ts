import { Component } from '@angular/core';
import { ExplodingCardType } from '../../model/exploding-card-type';
import { Location } from '@angular/common';

@Component({
  selector: 'app-help-page',
  templateUrl: './help-page.component.html',
})
export class HelpPageComponent {
  ExplodingCardType = ExplodingCardType;

  constructor(private location: Location) {}

  back() {
    this.location.back();
  }
}
