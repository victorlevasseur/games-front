import { Component, Input } from '@angular/core';
import { ExplodingCardType } from '../../../model/exploding-card-type';

@Component({
  selector: 'app-help-card',
  templateUrl: './help-card.component.html',
})
export class HelpCardComponent {
  @Input() title: string;
  @Input() quantity: string;
  @Input() types: ExplodingCardType[];
}
