import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfoService } from '../../../services/user-info.service';
import { GameInfo } from '../../../model/game-info';
import { ExplodingGameService } from '../../service/exploding-game.service';
import { ExplodingActionsService } from '../../service/exploding-actions.service';
import { ExplodingGameGuard } from '../../exploding-game.guard';

@Component({
  selector: 'app-exploding-game',
  templateUrl: './game-detail.component.html',
})
export class ExplodingGameDetailComponent {
  @Input()
  game: GameInfo;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userInfoService: UserInfoService,
    private gameService: ExplodingGameService,
    private actionsService: ExplodingActionsService,
    private gameGuard: ExplodingGameGuard,
    private router: Router
  ) {}

  onJoin(game: GameInfo) {
    this.gameService.gameId = game.guuid;
    this.actionsService
      .updateUser(game.guuid, this.userInfoService.me)
      .subscribe(() => {
        this.router.navigate(['/exploding', 'game', game.guuid, 'players']);
      });
  }

  onReplay(game: GameInfo) {
    this.actionsService
      .replayUser(game.guuid, this.userInfoService.uuid)
      .subscribe((explodingGame) => {
        this.gameGuard.redirect(explodingGame, game.guuid);
      });
  }
}
