import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserInfoService } from '../../../services/user-info.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ExplodingNotifiedPlayer } from '../../model/exploding-notified-player';
import { ExplodingGameService } from '../../service/exploding-game.service';
import { ExplodingActionsService } from '../../service/exploding-actions.service';
import { ExplodingGameGuard } from '../../exploding-game.guard';

@Component({
  selector: 'app-exploding-players',
  templateUrl: './exploding-players.component.html',
  styleUrls: ['./exploding-players.component.sass'],
})
export class ExplodingPlayersComponent implements OnInit, OnDestroy {
  me: ExplodingNotifiedPlayer;
  players: ExplodingNotifiedPlayer[];
  guuid: string;
  name: string;
  canStart = false;

  private gameSub: Subscription;

  constructor(
    private gameService: ExplodingGameService,
    private userInfoService: UserInfoService,
    private actionsService: ExplodingActionsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private gameGuard: ExplodingGameGuard
  ) {}

  ngOnInit(): void {
    this.me = <ExplodingNotifiedPlayer>{ ...this.userInfoService.me };
    this.guuid = this.activatedRoute.snapshot.params.guuid;
    this.gameSub = this.gameService.game$.subscribe((game) => {
      this.name = game.name;
      this.gameGuard.redirect(game, this.guuid);
      this.players = [...game.players];
      // this.me = game.players.find((s) => s.self);
      this.canStart = game.players.length > 1;
    });
  }

  ngOnDestroy(): void {
    this.gameSub.unsubscribe();
  }

  onStartGame(): void {
    this.actionsService.launchGame(this.guuid, this.me.uuid).subscribe();
  }

  onShufflePlayers() {
    this.actionsService.shufflePlayers(this.guuid, this.me.uuid).subscribe();
  }
}
