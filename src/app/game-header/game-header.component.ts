import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-game-header',
  templateUrl: './game-header.component.html',
})
export class GameHeaderComponent {
  @Input()
  name: string;
}
