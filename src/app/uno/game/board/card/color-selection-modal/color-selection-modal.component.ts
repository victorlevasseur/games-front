import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ALL_COLORS, UnoColor } from '../../../../model/uno-color';
import { Component } from '@angular/core';

@Component({
  selector: 'app-color-selection-modal',
  templateUrl: './color-selection-modal.component.html',
  styleUrls: ['./color-selection-modal.component.sass'],
})
export class ColorSelectionModalComponent {
  ALL_COLORS = ALL_COLORS;

  constructor(public modal: NgbActiveModal) {}

  onColorClicked(color: UnoColor) {
    this.modal.close(color);
  }
}
