import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfoService } from '../../../services/user-info.service';
import { UnoGameGuard } from '../uno-game.guard';
import { UnoGameService } from '../../services/uno-game.service';
import { UnoActionsService } from '../../services/uno-actions.service';
import { UnoGame } from '../../model/uno-game';
import { UnoPlayer } from '../../model/uno-player';
import { UnoColor } from '../../model/uno-color';
import { UnoCardType } from '../../model/uno-card-type';
import { UnoCard } from '../../model/uno-card';
import { User } from '../../../model/user';

@Component({
  selector: 'app-uno-board',
  templateUrl: './uno-board.component.html',
  styleUrls: ['./uno-board.component.sass'],
})
export class UnoBoardComponent implements OnInit, OnDestroy {
  guuid: string;
  game: UnoGame;
  me: UnoPlayer;

  canUno = false;
  canCounter = false;
  canSkip = false;
  hasAlreadySkip = false;
  score = 0;

  private gameSub: Subscription;
  private timeoutHandler: number;
  readonly colorSettings = {
    //https://www.color-meanings.com/wp-content/uploads/color-theory-quick-reference-guide-1470x1536.webp
    square1: {
      GREEN: '#70C700',
      YELLOW: '#FF9300',
      BLUE: '#0074BF',
      RED: '#D2278D',
    },
    square2: {
      GREEN: '#00B600',
      YELLOW: '#FFD000',
      BLUE: '#0229BA',
      RED: '#F22540',
    },
    square3: {
      GREEN: '#BADF00',
      YELLOW: '#FF7900',
      BLUE: '#009BCC',
      RED: '#C82DB3',
    },
    colorBlind: {
      GREEN: '#117733',
      YELLOW: '#DDCC77',
      BLUE: '#88CCEE',
      RED: '#882255',
    },
    flashy: {
      GREEN: '#15BB15',
      YELLOW: '#FFEE2E',
      BLUE: '#0C0CED',
      RED: '#FF1010',
    },
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private actionsService: UnoActionsService,
    private gameService: UnoGameService,
    private userInfoService: UserInfoService,
    private gameGuard: UnoGameGuard
  ) {}

  ngOnInit(): void {
    this.guuid = this.activatedRoute.snapshot.params.guuid;
    this.onChangeColor(this.userInfoService.me.colorSetting);
    this.gameSub = this.gameService.game$.subscribe((game) => {
      this.game = game;
      this.canUno = game.canUno;
      this.score = game.cards.reduce((sum, card) => {
        let added;
        switch (card.type) {
          case UnoCardType.JOKER:
          case UnoCardType.PLUS_FOUR:
            added = 50;
            break;
          case UnoCardType.NUMBER:
            added = card.number;
            break;
          default:
            added = 20;
        }
        return sum + added;
      }, 0);

      const canCounter = game.canCounter;
      if (!canCounter) {
        if (this.timeoutHandler != null) {
          clearTimeout(this.timeoutHandler);
          this.timeoutHandler = null;
        }
        this.canCounter = false;
      }
      if (canCounter) {
        this.timeoutHandler = setTimeout(() => {
          this.canCounter = true;
        }, 750);
      }
      this.canSkip = game.players[0].self && !this.hasAlreadySkip;
      this.hasAlreadySkip = this.hasAlreadySkip && game.players[0].self;
      this.me = game.players.find((p) => p.uuid === this.userInfoService.uuid);
      this.gameGuard.redirect(game, this.guuid);
    });
  }

  ngOnDestroy(): void {
    this.gameSub.unsubscribe();
  }

  onCardClicked(event: { cardId: string; color?: UnoColor }) {
    this.actionsService
      .playCard(this.guuid, this.me.uuid, event.cardId, event.color)
      .subscribe();
  }

  onSkip() {
    if (this.hasAlreadySkip || this.canSkip) {
      this.hasAlreadySkip = true;
      this.actionsService.skip(this.guuid, this.me.uuid).subscribe();
    }
  }

  onSayUno() {
    if (this.canUno) {
      this.actionsService.sayUno(this.guuid, this.me.uuid).subscribe();
    }
  }

  onCounterUno() {
    if (this.canCounter) {
      this.actionsService.counter(this.guuid, this.me.uuid).subscribe();
    }
  }

  onChangeColor(colorName: string) {
    if (!(colorName in this.colorSettings)) {
      colorName = Object.keys(this.colorSettings)[0];
    }
    const colorSetting = this.colorSettings[colorName];
    const rootStyle = document.documentElement.style;
    rootStyle.setProperty('--uno-green', colorSetting.GREEN);
    rootStyle.setProperty('--uno-yellow', colorSetting.YELLOW);
    rootStyle.setProperty('--uno-blue', colorSetting.BLUE);
    rootStyle.setProperty('--uno-red', colorSetting.RED);
    this.userInfoService.me = {
      ...this.userInfoService.me,
      colorSetting: colorName,
    };
  }

  canPlay(card: UnoCard) {
    return (
      this.game.players[0].self &&
      (this.game.sum == 0 ||
        card.type == UnoCardType.PLUS_FOUR ||
        card.type == UnoCardType.PLUS_TWO)
    );
  }

  getCardCountOfPlayer = (player: User): string => {
    if ((player as any).nbOfCards != null) {
      return (player as any).nbOfCards;
    } else {
      return '';
    }
  };
}
