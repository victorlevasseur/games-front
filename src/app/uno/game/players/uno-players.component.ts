import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserInfoService } from '../../../services/user-info.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UnoPlayer } from '../../model/uno-player';
import { UnoGameGuard } from '../uno-game.guard';
import { UnoActionsService } from '../../services/uno-actions.service';
import { UnoGameService } from '../../services/uno-game.service';
import { User } from '../../../model/user';
import { ItemScoreInfo } from '../../../ui/standings-list/item-standing';

@Component({
  selector: 'app-uno-players',
  templateUrl: './uno-players.component.html',
  styleUrls: ['./uno-players.component.sass'],
})
export class UnoPlayersComponent implements OnInit, OnDestroy {
  me: UnoPlayer;
  players: UnoPlayer[];
  guuid: string;
  name: string;
  canStart = false;
  canShufflePlayers = false;

  playerToCompare: UnoPlayer | null = null;

  private gameSub: Subscription;

  constructor(
    private gameService: UnoGameService,
    private userInfoService: UserInfoService,
    private actionsService: UnoActionsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private gameGuard: UnoGameGuard
  ) {}

  ngOnInit(): void {
    this.me = <UnoPlayer>{ ...this.userInfoService.me };
    this.guuid = this.activatedRoute.snapshot.params.guuid;
    this.gameSub = this.gameService.game$.subscribe((game) => {
      this.name = game.name;
      this.gameGuard.redirect(game, this.guuid);
      this.players = [...game.players].sort(
        (p1, p2) => p1.statistics.score - p2.statistics.score
      );
      this.me = game.players.find((s) => s.self);
      if (this.playerToCompare != null) {
        // Update the player to compare (stats).
        const newPlayerToCompare = game.players.find(
          (player) => player.uuid === this.playerToCompare.uuid
        );
        this.playerToCompare = newPlayerToCompare ?? null;
      }
      this.canStart = game.players.length > 1;
      this.canShufflePlayers = game.canShufflePlayers;
    });
  }

  ngOnDestroy(): void {
    this.gameSub.unsubscribe();
  }

  onStartGame(): void {
    this.actionsService.launchGame(this.guuid, this.me.uuid).subscribe();
  }

  canReset(): boolean {
    return this.players.find((p) => p.statistics.score >= 500) != null;
  }

  onResetGame(): void {
    this.actionsService.reset(this.guuid, this.me.uuid).subscribe();
  }

  onShufflePlayers() {
    this.actionsService.shufflePlayers(this.guuid, this.me.uuid).subscribe();
  }

  convertUnoPlayer(user: UnoPlayer): ItemScoreInfo {
    return {
      uuid: user.uuid,
      name: user.name,
      score: user.statistics?.score ?? 0,
      scoreEvolution: user.statistics?.lastScore ?? 0,
    };
  }

  setComparison(user: UnoPlayer): void {
    if (
      this.playerToCompare != null &&
      this.playerToCompare.uuid === user.uuid
    ) {
      this.playerToCompare = null;
    } else if (user.uuid !== this.me.uuid) {
      this.playerToCompare = user;
    }
  }
}
