import { Component, Input, OnInit } from '@angular/core';
import { UnoPlayer } from '../../../model/uno-player';
import { UnoPlayerStatistic } from './uno-player-statistic';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-uno-players-stats',
  templateUrl: './uno-players-stats.component.html',
  styleUrls: ['./uno-players-stats.component.sass'],
})
export class UnoPlayersStatsComponent implements OnInit {
  statistics: UnoPlayerStatistic[];

  @Input()
  self: UnoPlayer;

  @Input()
  playerToCompare: UnoPlayer | null = null;

  constructor(private decimalPipe: DecimalPipe) {}

  ngOnInit(): void {
    this.statistics = [
      {
        name: 'Nombre de parties',
        value: (player: UnoPlayer) => player.statistics.nbOfGames,
        display: (player: UnoPlayer) => '' + player.statistics.nbOfGames,
      },
      {
        name: 'Moyenne de points par partie',
        value: (player) =>
          player.statistics.scoreSum / player.statistics.nbOfGames,
        display: (player) =>
          this.decimalPipe.transform(
            player.statistics.scoreSum / player.statistics.nbOfGames,
            '1.0-0'
          ),
      },
      {
        name: 'Nombre de parties gagnées',
        value: (player) => player.statistics.nbOfGamesWon,
        display: (player) =>
          `${player.statistics.nbOfGamesWon} (${Math.floor(
            (player.statistics.nbOfGamesWon / player.statistics.nbOfGames) * 100
          )}%)`,
      },
      {
        name: 'Nombre de cartes piochées par Joker',
        value: (player) => player.statistics.nbOfCardsForcePicked,
        display: (player) => '' + player.statistics.nbOfCardsForcePicked,
      },
      {
        name: 'Nombre de fois Uno',
        value: (player) => player.statistics.nbOfTimesUno,
        display: (player) => '' + player.statistics.nbOfTimesUno,
      },
      {
        name: 'Nombre de contre-Uno donnés',
        value: (player) => player.statistics.nbOfCounterGiven,
        display: (player) => '' + player.statistics.nbOfCounterGiven,
      },
      {
        name: 'Nombre maximum de cartes (partie)',
        value: (player) => player.statistics.maxNbOfCards,
        display: (player) => '' + player.statistics.maxNbOfCards,
      },
      {
        name: 'Nombre de cartes jouées (partie)',
        value: (player) => player.statistics.nbOfCardsPlayed,
        display: (player) => '' + player.statistics.nbOfCardsPlayed,
      },
    ];
  }
}
