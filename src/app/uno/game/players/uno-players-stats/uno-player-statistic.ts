import { UnoPlayer } from '../../../model/uno-player';

export interface UnoPlayerStatistic {
  name: string;
  value(player: UnoPlayer): number;
  display(player: UnoPlayer): string;
}
