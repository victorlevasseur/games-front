import { User } from '../../model/user';

export interface UnoPlayer extends User {
  self: boolean;
  nbOfCards: number;
  statistics: Statistics;
}

export interface Statistics {
  score: number;
  scoreSum: number;
  lastScore: number;

  nbOfGames: number;
  nbOfGamesWon: number;
  nbOfCardsForcePicked: number;
  nbOfTimesUno: number;
  nbOfCounterGiven: number;
  maxNbOfCards: number;
  nbOfCardsPlayed: number;
}
