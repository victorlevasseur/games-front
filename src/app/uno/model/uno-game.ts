import { UnoPlayer } from './uno-player';
import { UnoCard } from './uno-card';
import { UnoColor } from './uno-color';

export interface UnoGame {
  canShufflePlayers: boolean;
  name: string;
  currentPlayer: string;

  players: UnoPlayer[];
  fixedPlayers: UnoPlayer[];
  cards: UnoCard[];

  gameEnded: boolean;
  gameStarted: boolean;
  reversed: boolean;

  canUno: boolean;
  canCounter: boolean;
  sum: number;

  cardPlayed: UnoCard;
  selectedColor?: UnoColor;
}
