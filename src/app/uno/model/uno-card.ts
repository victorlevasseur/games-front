import { UnoColor } from './uno-color';
import { UnoCardType } from './uno-card-type';

export interface UnoCard {
  uuid: string;
  color: UnoColor;
  type: UnoCardType;
  number?: number;
}
