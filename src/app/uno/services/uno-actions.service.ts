import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { User } from '../../model/user';
import { UnoGame } from '../model/uno-game';
import { UnoColor } from '../model/uno-color';
import { UnoEvents } from '../model/uno-events';
import { DEFAULT_HEADER } from '../../model/http-header';
import { GameType, GameTypePath } from '../../model/game-type';

@Injectable({ providedIn: 'root' })
export class UnoActionsService {
  private readonly baseUrl: string;

  constructor(private httpClient: HttpClient) {
    this.baseUrl = environment.apiUrl + '/' + GameTypePath.get(GameType.UNO);
  }

  updateUser(guuid: string, user: User): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${user.uuid}/${UnoEvents.add_player}`,
      user,
      DEFAULT_HEADER
    );
  }

  replayUser(guuid: string, uuid: string): Observable<UnoGame> {
    return this.httpClient.get<UnoGame>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/replay`,
      DEFAULT_HEADER
    );
  }

  launchGame(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.launch_game}`,
      null,
      DEFAULT_HEADER
    );
  }

  reset(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.reset_score}`,
      null,
      DEFAULT_HEADER
    );
  }

  shufflePlayers(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.shuffle_players}`,
      null,
      DEFAULT_HEADER
    );
  }

  playCard(
    guuid: string,
    uuid: string,
    cardId: string,
    color?: UnoColor
  ): Observable<void> {
    const body = {
      uuid: cardId,
      color: color,
    };
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.play_card}`,
      body,
      DEFAULT_HEADER
    );
  }

  skip(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.cannot_play}`,
      null,
      DEFAULT_HEADER
    );
  }

  sayUno(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.uno}`,
      null,
      DEFAULT_HEADER
    );
  }

  counter(guuid: string, uuid: string): Observable<void> {
    return this.httpClient.post<void>(
      `${this.baseUrl}/game/${guuid}/user/${uuid}/${UnoEvents.counter}`,
      null,
      DEFAULT_HEADER
    );
  }
}
