import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { v4 as uuidv4 } from 'uuid';
import { User } from '../model/user';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserInfoService {
  private _me = new BehaviorSubject<User>(undefined);

  constructor(private cookieService: CookieService) {
    if (this.cookieService.check('game-user')) {
      this.me = JSON.parse(this.cookieService.get('game-user'));
    } else {
      this.me = {
        uuid: uuidv4(),
        colorSetting: 'square2',
      };
    }
  }

  get me$(): Observable<User> {
    return this._me.asObservable();
  }

  get me(): User {
    return this._me.getValue();
  }

  set me(me: User) {
    this._me.next(me);
    const now = new Date();
    this.cookieService.set(
      'game-user',
      JSON.stringify(me),
      new Date(now.setMonth(now.getMonth() + 6)),
      '/',
      window.location.hostname
    );
  }

  get uuid(): string {
    return this.me.uuid;
  }
}
