import { Injectable, OnDestroy } from '@angular/core';
import {
  InjectableRxStompConfig,
  RxStompService,
  rxStompServiceFactory,
} from '@stomp/ng2-stompjs';
import { IMessage, Message, StompHeaders } from '@stomp/stompjs';
import { Observable, Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { UserInfoService } from './user-info.service';

@Injectable({ providedIn: 'root' })
export class WebsocketProviderService implements OnDestroy {
  private readonly stompService: RxStompService;
  private subscriptions = <Subscription[]>[];

  constructor(private userInfoService: UserInfoService) {
    const conf: InjectableRxStompConfig = {
      // Which server?
      brokerURL: environment.wsUrl,

      // How often to heartbeat?
      // Interval in milliseconds, set to 0 to disable
      heartbeatIncoming: 0, // Typical value 0 - disabled
      heartbeatOutgoing: 20000, // Typical value 20000 - every 20 seconds

      // Wait in milliseconds before attempting auto reconnect
      // Set to 0 to disable
      // Typical value 500 (500 milli seconds)
      reconnectDelay: 200,

      // Will log diagnostics on console
      // It can be quite verbose, not recommended in production
      // Skip this key to stop logging to console
      debug: (msg: string): void => {
        // console.log(new Date(), msg);
      },
    };
    this.stompService = rxStompServiceFactory(conf);
  }

  watch(destination: string, headers?: StompHeaders): Observable<IMessage> {
    return this.stompService.watch(destination, headers);
  }

  watchUserRelated(
    destination: string,
    headers?: StompHeaders
  ): Observable<IMessage> {
    return this.stompService.watch(
      `/user/${this.userInfoService.uuid}${destination}`,
      headers
    );
  }

  addSubscription(
    path: string,
    consumer: (payload: Message) => void,
    onlyForUser = false
  ): void {
    if (!onlyForUser) {
      this.subscriptions.push(this.watch(path).subscribe(consumer));
    } else {
      this.subscriptions.push(this.watchUserRelated(path).subscribe(consumer));
    }
  }

  ngOnDestroy(): void {
    this.clearSubscriptions();
  }

  clearSubscriptions(): void {
    if (this.subscriptions != null && this.subscriptions.length > 0) {
      this.subscriptions.forEach((s) => s.unsubscribe());
    }
  }
}
