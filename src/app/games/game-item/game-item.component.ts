import { Component, Input, OnInit } from '@angular/core';
import { GameType } from 'src/app/model/game-type';
import { GameInfo } from '../../model/game-info';

@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.sass'],
})
export class GameItemComponent implements OnInit {
  @Input()
  game: GameInfo;

  @Input()
  isNew = false;

  GameType = GameType;

  constructor() {}

  ngOnInit(): void {}
}
