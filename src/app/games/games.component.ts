import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameInfo } from '../model/game-info';
import { HttpClient } from '@angular/common/http';
import { GameType, GameTypePath } from '../model/game-type';
import { DEFAULT_HEADER } from '../model/http-header';
import { environment } from '../../environments/environment';
import { merge, Subject, Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UserInfoService } from '../services/user-info.service';
import {
  animate,
  style,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';
import { slideItemIn, slideItemOut } from '../ui/common-animations';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.sass'],
  animations: [
    trigger('gameItem', [
      transition(':enter', [useAnimation(slideItemIn)]),
      transition(':leave', [useAnimation(slideItemOut)]),
    ]),
  ],
})
export class GamesComponent implements OnInit, OnDestroy {
  games: GameInfo[];
  guuid: string;
  filter: string = null;
  GameType = GameType;

  isCreatingNewGame = false;

  private sub: Subscription;

  private triggerRefreshSubject = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private userInfoService: UserInfoService,
    private httpClient: HttpClient
  ) {}

  ngOnInit(): void {
    this.games = this.activatedRoute.snapshot.data.games;
    this.sub = merge(timer(0, 5000), this.triggerRefreshSubject)
      .pipe(
        switchMap(() => {
          return this.httpClient.get<GameInfo[]>(
            `${environment.apiUrl}/games`,
            {
              ...DEFAULT_HEADER,
              params: { uuid: this.userInfoService.uuid },
            }
          );
        })
      )
      .subscribe((games) => {
        this.isCreatingNewGame = false;
        this.games = games;
      });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  onNewGame(type: GameType): void {
    this.isCreatingNewGame = true;
    this.httpClient
      .post<any>(
        `${environment.apiUrl}/${GameTypePath.get(type)}/game/`,
        null,
        DEFAULT_HEADER
      )
      .subscribe((json) => {
        this.guuid = json.guuid;
        // Ask to refresh the list.
        // Cannot add directly into the list as it may be different
        // from the server's games sorted list.
        this.triggerRefreshSubject.next();
      });
  }

  gameTrackBy(index: number, game: GameInfo): string {
    return game.guuid;
  }
}
