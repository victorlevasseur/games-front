import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { TeamsComponent } from './code-names/game/teams/teams.component';
import { BoardComponent } from './code-names/game/board/board.component';
import { GameGuard } from './code-names/game/game.guard';
import { UnoGameGuard } from './uno/game/uno-game.guard';
import { UnoPlayersComponent } from './uno/game/players/uno-players.component';
import { UnoBoardComponent } from './uno/game/board/uno-board.component';
import { GamesComponent } from './games/games.component';
import { UserGuard } from './user.guard';
import { ExplodingGameGuard } from './exploding/exploding-game.guard';
import { ExplodingPlayersComponent } from './exploding/game/players/exploding-players.component';
import { ExplodingBoardComponent } from './exploding/game/board/exploding-board.component';
import { HelpPageComponent } from './exploding/game/help-page/help-page.component';

const routes: Routes = [
  {
    path: '',
    component: RegisterComponent,
  },
  {
    path: 'games',
    component: GamesComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'code-names',
    data: {
      backToGamesList: true,
    },
    children: [
      {
        path: 'game/:guuid/players',
        canActivate: [GameGuard],
        component: TeamsComponent,
      },
      {
        path: 'game/:guuid/board',
        canActivate: [GameGuard],
        component: BoardComponent,
      },
    ],
  },
  {
    path: 'uno',
    data: {
      backToGamesList: true,
    },
    children: [
      {
        path: 'game/:guuid/players',
        canActivate: [UnoGameGuard],
        component: UnoPlayersComponent,
      },
      {
        path: 'game/:guuid/board',
        canActivate: [UnoGameGuard],
        component: UnoBoardComponent,
      },
    ],
  },
  {
    path: 'exploding',
    data: {
      backToGamesList: true,
    },
    children: [
      {
        path: 'game/:guuid/players',
        canActivate: [ExplodingGameGuard],
        component: ExplodingPlayersComponent,
      },
      {
        path: 'game/:guuid/board',
        canActivate: [ExplodingGameGuard],
        component: ExplodingBoardComponent,
      },
      {
        path: 'help',
        component: HelpPageComponent,
      },
    ],
  },
  // otherwise redirect to home
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
