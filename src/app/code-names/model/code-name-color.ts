export enum CodeNameColor {
  NOT_PLAYED = 'NOT_PLAYED',
  NEUTRAL = 'NEUTRAL',
  RED = 'RED',
  BLUE = 'BLUE',
  BLACK = 'BLACK',
}
