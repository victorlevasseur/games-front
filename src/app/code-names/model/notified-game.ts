import { Spy } from './spy';
import { CodeNameColor } from './code-name-color';
import { NotifiedEnd } from './notified-end';
import { NotifiedCodeName } from './notified-code-name';
import { NotifiedHint } from './notified-hint';

export interface NotifiedGame {
  name: string;
  canSubmitHint: boolean;
  canSubmitGuess: boolean;
  canEndTurn: boolean;
  canStartGame: boolean;
  turn: CodeNameColor;

  missing: { first: CodeNameColor; second: number }[];
  spies: Spy[];
  ending: NotifiedEnd;
  codeNames: NotifiedCodeName[];
  hint: NotifiedHint;
}
