export interface RequestHint {
  hint: string;
  numberOfNames: number;
}
