export interface NotifiedHint {
  hint: string;
  numberOfNames: number;
  icons: string[][];
}
