import { Component, Input } from '@angular/core';
import { Spy } from '../../../../model/spy';
import { CodeNameColor } from '../../../../model/code-name-color';

@Component({
  selector: 'app-spy',
  templateUrl: './spy.component.html',
})
export class SpyComponent {
  @Input('spy') spy: Spy;

  CodeNameColor = CodeNameColor;
}
