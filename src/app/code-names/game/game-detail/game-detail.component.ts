import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionsService } from '../../services/actions.service';
import { CodeNameColor } from '../../model/code-name-color';
import { UserInfoService } from '../../../services/user-info.service';
import { GameService } from '../../services/game.service';
import { GameInfo } from '../../../model/game-info';
import { GameGuard } from '../game.guard';

@Component({
  selector: 'app-code-name-game',
  templateUrl: './game-detail.component.html',
})
export class CodeNamesGameDetailComponent {
  @Input()
  game: GameInfo;

  CodeNameColor = CodeNameColor;

  constructor(
    private activatedRoute: ActivatedRoute,
    private actionsService: ActionsService,
    private userInfoService: UserInfoService,
    private gameService: GameService,
    private router: Router,
    private gameGuard: GameGuard
  ) {}

  onJoin(game: GameInfo, color: CodeNameColor) {
    let user = { ...this.userInfoService.me, team: color, spyMaster: false };
    this.gameService.gameId = game.guuid;
    this.actionsService.updateUser(game.guuid, user).subscribe(() => {
      this.router.navigate(['/code-names', 'game', game.guuid, 'players']);
    });
  }

  onReplay(game: GameInfo) {
    this.actionsService
      .replayUser(game.guuid, this.userInfoService.uuid)
      .subscribe((notifiedGame) => {
        this.gameGuard.redirect(notifiedGame, game.guuid);
      });
  }
}
