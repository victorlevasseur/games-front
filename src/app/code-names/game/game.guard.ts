import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GameService } from '../services/game.service';
import { ActionsService } from '../services/actions.service';
import { UserInfoService } from '../../services/user-info.service';
import { map } from 'rxjs/operators';
import { NotifiedGame } from '../model/notified-game';

@Injectable()
export class GameGuard implements CanActivate {
  constructor(
    private gameService: GameService,
    private actionsService: ActionsService,
    private userInfoService: UserInfoService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> {
    if (this.gameService.gameId != null) {
      return of(true);
    } else {
      const guuid = route.params.guuid;
      return this.actionsService
        .replayUser(guuid, this.userInfoService.uuid)
        .pipe(
          map((notifiedGame) => {
            if (notifiedGame != null) {
              this.gameService.gameId = guuid;
              this.gameService.publishGame(notifiedGame);
            }
            return this.router.createUrlTree(
              this.buildRedirect(notifiedGame, guuid)
            );
          })
        );
    }
  }

  redirect(notifiedGame: NotifiedGame, guuid): void {
    this.router.navigate(this.buildRedirect(notifiedGame, guuid));
  }

  private buildRedirect(notifiedGame: NotifiedGame, guuid: string): any[] {
    if (notifiedGame != null) {
      if (notifiedGame.codeNames == null) {
        return ['/code-names', 'game', guuid, 'players'];
      } else {
        return ['/code-names', 'game', guuid, 'board'];
      }
    } else {
      return ['/code-names', 'games'];
    }
  }
}
