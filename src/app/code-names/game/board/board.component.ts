import { Component, OnDestroy, OnInit } from '@angular/core';
import { CodeNameColor } from '../../model/code-name-color';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from '../../services/game.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActionsService } from '../../services/actions.service';
import { UserInfoService } from '../../../services/user-info.service';
import { NotifiedGame } from '../../model/notified-game';
import { GameGuard } from '../game.guard';
import { Spy } from '../../model/spy';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationModalComponent } from './validation-modal/validation-modal.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.sass'],
})
export class BoardComponent implements OnInit, OnDestroy {
  CodeNameColor = CodeNameColor;
  guuid: string;
  me: Spy;
  game: NotifiedGame;
  formGroup: FormGroup;

  private gameSub: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private actionsService: ActionsService,
    private gameService: GameService,
    private userInfoService: UserInfoService,
    private fb: FormBuilder,
    private gameGuard: GameGuard,
    private _modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.guuid = this.activatedRoute.snapshot.params.guuid;
    this.gameSub = this.gameService.game$.subscribe((game) => {
      this.game = game;
      this.me = game.spies.find((s) => s.uuid === this.userInfoService.uuid);
      if (game.canSubmitHint) {
        this.initFormGroup();
      } else {
        this.formGroup = null;
      }
      this.gameGuard.redirect(game, this.guuid);
    });
  }

  ngOnDestroy(): void {
    this.gameSub.unsubscribe();
  }

  onGiveHint(): void {
    if (this.formGroup.valid) {
      this.actionsService
        .submitHint(this.guuid, this.userInfoService.uuid, this.formGroup.value)
        .subscribe();
    }
  }

  private initFormGroup() {
    this.formGroup = this.fb.group({
      hint: [null, Validators.required],
      numberOfNames: [
        null,
        [Validators.required, Validators.min(1), Validators.max(9)],
      ],
    });
  }

  onNamesSelected(number: number) {
    if (this.game.canSubmitGuess) {
      this.actionsService
        .submitGuess(this.guuid, this.userInfoService.uuid, number)
        .subscribe();
    }
  }

  onNextTurnGame() {
    this._modalService
      .open(ValidationModalComponent)
      .result.then((value: boolean) => {
        if (value) {
          this.actionsService
            .nextTurn(this.guuid, this.userInfoService.uuid)
            .subscribe();
        }
      });
  }

  onResetGame() {
    this._modalService
      .open(ValidationModalComponent)
      .result.then((value: boolean) => {
        if (value) {
          this.actionsService
            .startNewGame(this.guuid, this.userInfoService.uuid)
            .subscribe(() => {
              this.router.navigate(['../players'], {
                relativeTo: this.activatedRoute,
              });
            });
        }
      });
  }
}
