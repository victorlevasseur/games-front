import { Injectable } from '@angular/core';
import { Message } from '@stomp/stompjs';
import { BehaviorSubject, Observable } from 'rxjs';
import { WebsocketProviderService } from '../../services/websocket-provider.service';
import { filter } from 'rxjs/operators';
import { NotifiedGame } from '../model/notified-game';

@Injectable({ providedIn: 'root' })
export class GameService {
  private readonly game;

  readonly game$: Observable<NotifiedGame>;
  private guuid: string;

  constructor(private webSocketService: WebsocketProviderService) {
    this.game = new BehaviorSubject<NotifiedGame>(null);
    this.game$ = this.game
      .asObservable()
      .pipe(filter((value) => value != null));
  }

  publishGame(game: NotifiedGame): void {
    this.game.next(game);
  }

  set gameId(guuid: string) {
    this.guuid = guuid;
    this.webSocketService.clearSubscriptions();
    this.webSocketService.addSubscription(
      `/code-names/game/${guuid}`,
      (payload: Message) => {
        this.game.next(JSON.parse(payload.body));
      },
      true
    );
  }

  get gameId(): string {
    return this.guuid;
  }
}
