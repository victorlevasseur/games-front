import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserInfoService } from '../services/user-info.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass'],
})
export class RegisterComponent implements OnInit {
  icons = [
    'book',
    'trophy',
    'watch',
    'wrench',
    'star',
    'music-note-beamed',
    'lightning',
    'house-door',
    'heart',
    'hammer',
    'gift',
    'gem',
    'film',
    'lightbulb',
    'cloud',
    'camera-video',
    'briefcase',
    'alarm',
    'controller',
    'megaphone',
    'truck',
    'tornado',
    'puzzle',
    'mouse3',
  ];
  formGroup: FormGroup;

  constructor(
    private userInfoService: UserInfoService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    const me = this.userInfoService.me;

    this.formGroup = this.fb.group({
      name: [me.name, Validators.required],
      icon: [me.icon, Validators.required],
    });
  }

  onSubmit() {
    if (this.formGroup.valid) {
      this.userInfoService.me = {
        ...this.userInfoService.me,
        ...this.formGroup.value,
      };
      this.router.navigate(['/games']);
    }
  }
}
