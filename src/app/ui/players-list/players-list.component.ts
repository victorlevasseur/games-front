import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { User } from '../../model/user';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.sass'],
  animations: [
    trigger('currentPlayer', [
      state(
        'no',
        style({
          transform: 'translateY(-24px)',
        })
      ),
      state(
        'yes',
        style({
          transform: 'translateY(0)',
        })
      ),
      transition('no => yes', [animate('300ms ease-in-out')]),
      transition('yes => no', [
        animate(
          '300ms ease-in-out',
          style({
            transform: 'translateY(24px)',
          })
        ),
      ]),
    ]),
  ],
})
export class PlayersListComponent implements OnInit, OnChanges {
  /**
   * The list of players.
   * The players must be ordered correctly and
   * the first one is the player taking its turn.
   */
  @Input()
  players: User[];

  /**
   * UUID of self.
   */
  @Input()
  selfUuid: string;

  /**
   * An optional function returning an extra data to display next
   * to each player names.
   */
  @Input()
  extraInfo?: (player: User) => string;

  stablePlayers: User[];

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!!changes.players || !!changes.selfUuid) {
      const positionOfSelf = this.players.findIndex(
        (user) => user.uuid === this.selfUuid
      );
      this.stablePlayers = [
        ...this.players.slice(positionOfSelf),
        ...this.players.slice(0, positionOfSelf),
      ];
    }
  }

  trackByPlayer(player: User): string {
    return player.uuid;
  }
}
