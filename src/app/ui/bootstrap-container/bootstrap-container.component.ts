import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-bootstrap-container',
  template: ` <ng-content></ng-content> `,
  styles: [':host { display: block; margin-top: 64px; }'],
})
export class BootstrapContainerComponent implements OnInit {
  @HostBinding('class.container') container = true;

  constructor() {}

  ngOnInit(): void {}
}
