import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapContainerComponent } from './bootstrap-container/bootstrap-container.component';
import { CardContainerComponent } from './card-container/card-container.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ConcatStringArrayPipe } from './pipes/comma-separated-list.pipe';
import { GameTypePipe } from './pipes/game-type.pipe';
import { PlayersListComponent } from './players-list/players-list.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StandingsListComponent } from './standings-list/standings-list.component';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [
    BootstrapContainerComponent,
    CardContainerComponent,
    ConcatStringArrayPipe,
    GameTypePipe,
    PlayersListComponent,
    StandingsListComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FlexLayoutModule,
    InlineSVGModule,
    MatIconModule,
    MatTableModule,
  ],
  exports: [
    BootstrapContainerComponent,
    CardContainerComponent,
    ConcatStringArrayPipe,
    GameTypePipe,
    PlayersListComponent,
    StandingsListComponent,
  ],
})
export class UiModule {}
