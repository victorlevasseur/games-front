import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'concatStringArray',
})
export class ConcatStringArrayPipe implements PipeTransform {
  transform(value: string[] | null | undefined, ...args: unknown[]): unknown {
    if (value != null) {
      return value.join(', ');
    } else {
      return '';
    }
  }
}
