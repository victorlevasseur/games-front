import { Pipe, PipeTransform } from '@angular/core';
import { GameType } from '../../model/game-type';

@Pipe({
  name: 'gameType',
})
export class GameTypePipe implements PipeTransform {
  transform(value: GameType, ...args: unknown[]): unknown {
    switch (value) {
      case GameType.CODE_NAMES:
        return 'Codenames';
      case GameType.EXPLODING:
        return 'Exploding Kittens';
      case GameType.UNO:
        return 'Uno';
    }
  }
}
