import { Component, Input, OnInit } from '@angular/core';
import { transition, trigger, useAnimation } from '@angular/animations';
import { slideIn, slideOut } from '../common-animations';

@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition('void => animated', [useAnimation(slideIn)]),
      transition('animated => void', [useAnimation(slideOut)]),
    ]),
  ],
})
export class CardContainerComponent implements OnInit {
  @Input()
  maxWidth: string;

  @Input()
  animated = true;

  constructor() {}

  ngOnInit(): void {}
}
