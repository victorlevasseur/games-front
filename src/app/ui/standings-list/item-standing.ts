export type ItemToStanding<Item> = (item: Item) => ItemScoreInfo;

export interface ItemScoreInfo {
  uuid: string;

  name: string;

  /**
   * Total score.
   */
  score: number;

  /**
   * Score evolution since last game.
   */
  scoreEvolution: number;
}
