import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ItemToStanding } from './item-standing';

interface ItemStanding {
  uuid: string;

  name: string;

  /**
   * 0 is first.
   */
  position: number;

  /**
   * Total score
   */
  score: number;

  /**
   * Score evolution since last game.
   */
  scoreEvolution: number;

  /**
   * Number of position gained since last game.
   * Negative number = positions lost.
   */
  positionGained: number;
}

@Component({
  selector: 'app-standings-list',
  templateUrl: './standings-list.component.html',
  styleUrls: ['./standings-list.component.sass'],
})
export class StandingsListComponent<ItemType> implements OnInit, OnChanges {
  @Input()
  items: ItemType[];

  @Input()
  selfUuid: string;

  @Input()
  scoreComparison: 'lower_is_better' | 'higher_is_better';

  @Input()
  itemToStandingFn: ItemToStanding<ItemType>;

  @Output()
  itemClick = new EventEmitter<ItemType>();

  computedItems: ItemStanding[];

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    const itemsScoresData = this.items.map(this.itemToStandingFn);

    const itemsScoresDataSorted = itemsScoresData
      .slice()
      .sort(
        this.scoreComparison === 'lower_is_better'
          ? (a, b) => a.score - b.score
          : (a, b) => b.score - a.score
      );

    const itemsScoresDataSortedByPreviousScore = itemsScoresData
      .slice()
      .sort(
        this.scoreComparison === 'lower_is_better'
          ? (a, b) => a.score - a.scoreEvolution - b.score + b.scoreEvolution
          : (a, b) => b.score - b.scoreEvolution - a.score + a.scoreEvolution
      );

    // FIXME: Ties are not handled properly for now
    // FIXME: Players in ties will be ordered (and have a different standing)
    // FIXME: according to the server's data ordering.

    this.computedItems = itemsScoresDataSorted.map((item, index) => {
      const oldScoreIndex = itemsScoresDataSortedByPreviousScore.findIndex(
        (oldItem) => oldItem.uuid === item.uuid
      );
      const positionGained = oldScoreIndex - index;
      return {
        ...item,
        position: index,
        positionGained,
      };
    });
  }

  onItemStandingClicked(itemStanding: ItemStanding): void {
    const matchingItem = this.items.find(
      (item) => this.itemToStandingFn(item).uuid === itemStanding.uuid
    );
    if (matchingItem) {
      this.itemClick.emit(matchingItem);
    }
  }
}
