import { animate, animation, style } from '@angular/animations';

export const slideIn = animation([
  style({
    opacity: 0,
    transform: 'translateY(-50px)',
  }),
  animate(
    '300ms ease-out',
    style({
      opacity: 1,
      transform: 'translateY(0px)',
    })
  ),
]);

export const slideOut = animation([
  style({
    opacity: 1,
    transform: 'translateY(0)',
  }),
  animate(
    '300ms ease-out',
    style({
      opacity: 0,
      transform: 'translateY(50px)',
    })
  ),
]);

export const slideItemIn = animation([
  style({
    opacity: 0,
    transform: 'translateX(-50px)',
    height: 0,
    overflow: 'hidden',
    paddingTop: 0,
    paddingBottom: 0,
  }),
  animate(
    '100ms ease-in-out',
    style({
      opacity: 0,
      transform: 'translateX(-50px)',
      height: '*',
      overflow: 'visible',
    })
  ),
  animate(
    '200ms ease-out',
    style({
      opacity: 1,
      transform: 'translateX(0)',
    })
  ),
]);

export const slideItemOut = animation([
  style({
    opacity: 1,
    transform: 'translateX(0)',
  }),
  animate(
    '200ms ease-out',
    style({
      opacity: 0,
      transform: 'translateX(50px)',
      height: '*',
      overflow: 'visible',
    })
  ),
  animate(
    '100ms ease-in-out',
    style({
      opacity: 0,
      transform: 'translateX(50px)',
      height: 0,
      overflow: 'hidden',
      paddingTop: 0,
      paddingBottom: 0,
    })
  ),
]);
