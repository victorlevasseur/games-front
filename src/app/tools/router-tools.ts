import { ActivatedRouteSnapshot } from '@angular/router';

export class RouterTools {
  public static getRecursiveData(route: ActivatedRouteSnapshot): any {
    let data = route.data;
    if (route.children.length > 0) {
      return {
        ...data,
        ...RouterTools.getRecursiveData(route.children[0]),
      };
    } else {
      return data;
    }
  }
}
